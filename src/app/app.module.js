angular.module('teleopti-pizza', [
    'scheduleModule',
    'ngTouch',
    'ngAnimate',
    'ngRoute',
    'ui.bootstrap',
    'attendeesModule'
]);