(function(){
    function appConfig($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');
        
        $routeProvider.
        when('/list', {
            template: '<schedule></schedule>'
        }).
        when('/list/view',{
            template: '<attendees></attendees>'
        }).
        otherwise('/list');
    };

    appConfig.$inject = ['$locationProvider', '$routeProvider'];

    angular.module('teleopti-pizza').config(appConfig);
})();