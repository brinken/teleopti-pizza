(function(){
    function ScheduleController(scheduleService, $location, attendeesService){
        var self = this;

        self.error = true;
        self.errorMessage = 'Loading data...';
        self.data = {};
        self.amountOfTeamMembers = '';
        self.madeASearch = false;
        self.breakTimes = [
            {
                color: "#FF0000",
                description: "Short break"
            },
            {
                color: "#FFFF00",
                description: "Lunch"
            }
            
        ];
        self.earliestStartTime = '';
        self.latestStartTime = '';
        self.workforce = [];

        scheduleService.getSchedules().then(function(response){
                self.error = false;
                self.data = response;
            }, function(response){
                self.error = true;
                self.errorMessage = 'Server responded with error ' + response;
            }
        );

        self.checkForTimeSlot = function(){
            var scheduledWorkers = self.getWorkersWithContractTime(self.data.ScheduleResult.Schedules);
            scheduledWorkers = self.controlDate(scheduledWorkers);
            scheduledWorkers = self.removeBreakTimesAndSetEarliestAndLatestStartTime(scheduledWorkers);
            self.workforce = self.checkWorkforceEveryFifteenMinute(scheduledWorkers);
            self.madeASearch = true;
        };

        self.controlDate = function(scheduledWorkers){
            var timeStampForDate = 1450051200000;
            var cleanDate = '';
            angular.forEach(scheduledWorkers, function(worker, key){
                cleanDate = self.cleanDateString(worker.Date);
                
                if(cleanDate !== timeStampForDate){
                    scheduledWorkers.splice(key, 1);
                }
            });

            return scheduledWorkers;
        };

        self.checkWorkforceEveryFifteenMinute = function(scheduledWorkers){
            var time = self.earliestStartTime;
            var fifteenMinutes = self.minutesToMilliseconds(15);
            var workerListAndTimes = [];
            while(time <= self.latestStartTime){
                var workerCount = 0;
                var isWorking = false;
                var workerList = [];
                angular.forEach(scheduledWorkers, function(worker, key){
                    isWorking = self.checkIfWorking(worker, time);
                    if(isWorking){
                        workerCount++;
                        workerList.push(worker);
                    }
                });
                if(workerCount >= self.amountOfTeamMembers){
                    workerListAndTimes.push(
                        {
                            time: time,
                            worker_count: workerCount,
                            employeer_list: workerList
                        }
                    );
                }

                time = time + fifteenMinutes;
            }

            return workerListAndTimes;
        };

        self.minutesToMilliseconds = function(minutes){
            return minutes * 60 * 1000;
        };

        self.checkIfWorking = function(worker, time){
            var startTime = '';
            var endTime = '';
            var isWorking = false;
            angular.forEach(worker.Projection, function(projection, projectionKey){
                startTime = self.cleanDateString(projection.Start);
                endTime = startTime + self.minutesToMilliseconds(projection.minutes);
                if(time >= startTime && time <= endTime){
                    isWorking = true;
                }
            });

            return isWorking;
        };

        self.removeBreakTimesAndSetEarliestAndLatestStartTime = function(scheduledWorkers){
            angular.forEach(scheduledWorkers, function(worker, key){
                angular.forEach(worker.Projection, function(projection, projectionKey){
                    var parsedTimeStamp = self.cleanDateString(projection.Start);
                    var isBreakTime = self.checkIfBreakTime(projection);

                    if(!self.earliestStartTime || parsedTimeStamp < self.earliestStartTime){
                        self.earliestStartTime = parsedTimeStamp;
                    }

                    if(parsedTimeStamp > self.latestStartTime){
                        self.latestStartTime = parsedTimeStamp;
                    }
                
                    if(isBreakTime){
                        worker.Projection.splice(projectionKey, 1);
                    }
                });
            });

            return scheduledWorkers;
        };

        self.checkIfBreakTime = function(projection){
            var isBreakTime = false;
            angular.forEach(self.breakTimes, function(breakTime, key){
                if(breakTime.color === projection.Color){
                    isBreakTime = true;
                }
            });

            return isBreakTime;
        }

        self.cleanDateString = function(dateString){
            var cleanDate = dateString.replace('/Date(', '');
            cleanDate = cleanDate.replace('+0000)/', '');
            return parseInt(cleanDate);
        };

        self.getWorkersWithContractTime = function(schedules){
            var workerList = [];
            angular.forEach(schedules, function(schedule, key){
                if(schedule.ContractTimeMinutes > 0){
                    this.push(schedule);
                }
            }, workerList);

            return workerList;
        };

        self.getUnixTimeStampForTime = function(time){
            var date = new Date(time);
            return date.toUTCString();
        };

        self.showAttendees = function(workforce){
            attendeesService.data.workforce = workforce;
            $location.path('/list/view');
        };
    }

    ScheduleController.$inject = ['scheduleService', '$location', 'attendeesService'];

    angular.module('scheduleModule').component('schedule', {
        templateUrl: 'template/schedule.template.html',
        controller: ScheduleController
    });
})();
