(function(){
    function scheduleService($http, $sce){
        var self = this;

        self.getSchedules = function(){
            var url = 'api/index.php';
            return $http.get(url).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return response.status;
            });
        }

    }

    scheduleService.$inject = ['$http', '$sce'];

    angular.module('scheduleModule').service('scheduleService', scheduleService);
})();