(function(){
    function attendeesService(){
        var self = this;
        self.data = {workforce: []};
    }

    attendeesService.$inject = [];

    angular.module('attendeesModule').service('attendeesService', attendeesService);
})();