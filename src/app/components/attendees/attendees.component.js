(function(){
    function attendeesController(attendeesService, $location){
        var self = this;
        self.employeeList = [];
        if(attendeesService.data.workforce.length === 0){
            $location.path('/list');
        }

        self.employeeList = attendeesService.data.workforce.employeer_list;

        self.backToList = function(){
            $location.path('/list');
        };
    }

    attendeesController.$inject = ['attendeesService', '$location'];

    angular.module('attendeesModule').component('attendees', {
        templateUrl: 'template/attendees.template.html',
        controller: attendeesController
    });
})();
