module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            js: {
                src:[
                    'src/app/*.module.js',
                    'src/app/*/*/*.module.js',
                    'src/app/*/*/*.service.js',
                    'src/app/*/*/*.component.js',
                    'src/app/*/*/*.controller.js',
                    'src/app/*.config.js'
                ],
                dest: 'src/app/app.js'
            }
        },
        uglify: {
            options: {
            banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: [
                    'src/app/*.module.js',
                    'src/app/*/*/*.module.js',
                    'src/app/*/*/*.service.js',
                    'src/app/*/*/*.component.js',
                    'src/app/*/*/*.controller.js',
                    'src/app/*.config.js'
                ],
                dest: 'build/app/app.min.js'
            }
        },
        processhtml: {
            dist: {
              files: {
                'build/index.html': ['src/index.html']
              }
            }
        },
        copy: {
            main: {
              files: [
                // includes files within path
                {expand: true, cwd: 'src/', src: ['template/*'], dest: 'build/', filter: 'isFile'},
                {expand: true, cwd: 'src/', src: ['api/*'], dest: 'build/', filter: 'isFile'},
                {expand: true, cwd: 'src/', src: ['css/*'], dest: 'build/', filter: 'isFile'}
              ],
            },
          }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-processhtml');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Default task(s).
    grunt.registerTask('default', ['concat', 'uglify', 'processhtml', 'copy']);

};